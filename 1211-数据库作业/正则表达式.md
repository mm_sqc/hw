

## 正则表达式的作用

正则表达式，无论在哪种语言里面，都表示一个对象。本质上就是通过一门特殊的语言，来描述我们的规则。通常具有下面两个功能。

1.表单校验(第一多)

2.匹配文本(搜索，过滤，大数据，爬虫)

创建正则表达式

```
var patt=new RegExp(pattern,modifiers); 

或者

 var patt=/pattern/modifiers; 
 
pattern（模式） 描述了表达式的模式(规则)
	模式其实就是描述正则正则表达式的规则。
	
modifiers(修饰符) 用于指定全局匹配、区分大小写的匹配和多行匹配
	i	执行对大小写不敏感的匹配。
	g	执行全局匹配（查找所有匹配而非在找到第一个匹配后停止）。
	m	执行多行匹配。
```

学习正则对象的方法

test:

​	这个方法，能够检测字符串是否符合我们的模式，如果符合，返回true，否则，返回false

exec:

​	这个方法能够让我们查找字符串中是否有匹配的字符串，有就返回该字符串，没有返回null



学习正则表达式的组成

方括号

​	方括号表示一位数的匹配规则。  [abc]		--		这一位的值可能是a，可能是b，可能是c

​	[a]						包含a就能够通过
​	[ab]					 包含a或者包含b都能通过

​	[a-z]					包含任意小写字母通过

​	[A-Za-z0-9]		包含任意数字，字母即可通过

​	[\u4e00-\u9fa5]	包含任意中文即可通过

小括号

小括号表示分组

​	（红色|黄色）		强制只能录入红色或者黄色，其他都不通过

元字符：

​	\w			 查找数字、字母及下划线。 		等价于[a-zA-Z0-9_]

​    \d			 查找所有的数字							 等价于[0-9]

​	\s				查找空白字符							 

​	\b			查找单词的边界

​     [\n](https://www.runoob.com/jsref/jsref-regexp-newline.html) 		 查找换行

​	\r			查找回车符

​    \t			查找制表符

   \v             查找垂直制表符

 边界：

​	^			表示开头

​	$			表示结尾

量词

​	+			表示至少一个          

​	*            任意个    

​    ？           表示0个或者1个       

​    {n}          表示n个

​	{n,}         至少n个    

​    {n,m}      位数必须在n到m之间 

​	is (?= all)		匹配is后面跟着空格all的内容

