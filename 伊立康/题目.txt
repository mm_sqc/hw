﻿题目：
1、查询所有的课程的名称以及对应的任课老师姓名
SELECT * FROM  course,teacher;
SELECT cname 课程名称 ,tname 老师 FROM course,teacher WHERE cid=tid;

2、查询学生表中男女生各有多少人
SELECT gender,COUNT(sid) FROM student GROUP BY gender;

3、查询物理成绩等于100的学生的姓名
SELECT sname FROM student,
(SELECT studentid FROM
(SELECT studentid,num FROM score WHERE courseid=(SELECT teacherid FROM course WHERE cname='物理'))ti WHERE num=100)tu WHERE
sid=tu.studentid;


4、查询平均成绩大于八十分的同学的姓名和平均成绩
SELECT sname,rt FROM 
(SELECT studentid,rt FROM
(SELECT studentid,AVG(num) rt  FROM score GROUP BY studentid)ii 
WHERE rt>=80)oo,student WHERE sid=oo.studentid;

5、查询所有学生的学号，姓名，选课数，总成绩
SELECT student.sid,student.sname,op.dd,op.tt FROM
(SELECT studentid,COUNT(studentid)dd,SUM(num) tt FROM score GROUP BY studentid)op,student WHERE sid=studentid;

6、 查询姓李老师的个数
SELECT COUNT(tname) FROM teacher WHERE tname LIKE '李%';

7、 查询没有报李平老师课的学生姓名
SELECT sname FROM student WHERE sid NOT IN
(SELECT studentid FROM 
(SELECT studentid FROM score WHERE courseid IN
(SELECT cid FROM course WHERE teacherid=
(SELECT tid FROM teacher WHERE tname='李平老师')))ee GROUP BY studentid);

8、 查询物理课程比生物课程高的学生的学号
SELECT xx.studentid FROM 
(SELECT studentid,courseid,num FROM score WHERE studentid IN
(SELECT studentid FROM
(SELECT studentid,COUNT(studentid) ir FROM 
(SELECT studentid,num FROM score WHERE courseid IN
(SELECT cid FROM course WHERE cname IN ('物理','生物')))op GROUP BY studentid)po WHERE ir=2) AND
courseid IN (SELECT cid FROM course WHERE cname='物理'))xx,

(SELECT studentid,courseid,num FROM score WHERE studentid IN
(SELECT studentid FROM
(SELECT studentid,COUNT(studentid) ir FROM 
(SELECT studentid,num FROM score WHERE courseid IN
(SELECT cid FROM course WHERE cname IN ('物理','生物')))op GROUP BY studentid)po WHERE ir=2) AND
courseid IN (SELECT cid FROM course WHERE cname='生物'))dd
WHERE xx.num>dd.num AND xx.studentid=dd.studentid;

9、 查询没有同时选修物理课程和体育课程的学生姓名
SELECT sname 学生姓名 FROM student WHERE sid NOT IN
(SELECT studentid FROM 
(SELECT studentid,COUNT(studentid) tt FROM
(SELECT studentid FROM score WHERE courseid IN
(SELECT cid FROM course WHERE cname IN ('物理','体育')))tu GROUP BY studentid)pu WHERE tt=2);

10、查询挂科超过两门(包括两门)的学生姓名和班级  分数小于60分

SELECT sname 姓名,calssid 班级 FROM student WHERE sid=
(SELECT studentid FROM 
(SELECT studentid,COUNT(studentid) id FROM 
(SELECT studentid FROM score WHERE num<=60)qi GROUP BY studentid)qo WHERE id=2);

11、查询选修了所有课程的学生姓名
SELECT sname FROM student WHERE sid IN
(SELECT studentid FROM
(SELECT studentid,COUNT(studentid) op FROM score GROUP BY studentid)pp
WHERE op=(SELECT COUNT(cid) rr FROM course));

12、查询李平老师教的课程的所有成绩记录
SELECT courseid,num FROM score WHERE courseid IN
(SELECT cid FROM course WHERE teacherid=
(SELECT tid FROM teacher WHERE tname='李平老师'));
 
13、查询全部学生都选修了的课程号和课程名
SELECT courseid 全选课程 FROM 
(SELECT courseid,COUNT(courseid)ee FROM score GROUP BY courseid)ii WHERE ee=(SELECT COUNT(sid) FROM student);

14、查询每门课程被选修的次数
SELECT course.cname,po.次数 FROM course,
(SELECT courseid oo,COUNT(courseid)次数 FROM score GROUP BY courseid)po WHERE oo=cid;

15、查询之选修了一门课程的学生姓名和学号
SELECT sid 学号,sname 姓名 FROM student,
(SELECT studentid FROM 
(SELECT studentid,COUNT(studentid) yy FROM score GROUP BY studentid)po WHERE yy=1)rr WHERE sid=studentid;

16、查询所有学生考出的成绩并按从高到低排序（成绩去重）
SELECT num 成绩 FROM 
(SELECT DISTINCT num FROM score)cj ORDER BY num DESC;


17、查询平均成绩大于85的学生姓名和平均成绩
SELECT sname,rt FROM 
(SELECT studentid,rt FROM
(SELECT studentid,AVG(num) rt  FROM score GROUP BY studentid)ii 
WHERE rt>85)oo,student WHERE sid=oo.studentid;

18、查询生物成绩不及格的学生姓名和对应生物分数
SELECT student.sname 姓名,bjg.num 分数 FROM student,
(SELECT studentid rr,num FROM score WHERE courseid=
(SELECT cid FROM course WHERE cname='生物') AND num<60)bjg WHERE rr=sid;



19、查询在所有选修了李平老师课程的学生中，这些课程(李平老师的课程，不是所有课程)平均成绩最高的学生姓名

SELECT sname 姓名 FROM student,
(SELECT oo,iii FROM (SELECT  oo,AVG(ii) iii FROM
(SELECT studentid oo,num ii FROM score WHERE courseid IN
(SELECT cid FROM course WHERE teacherid IN
(SELECT tid FROM teacher WHERE tname='李平老师')))zo GROUP BY oo)oi WHERE iii=
(SELECT MAX(iii) iio FROM 
(SELECT  oo,AVG(ii) iii FROM
(SELECT studentid oo,num ii FROM score WHERE courseid IN
(SELECT cid FROM course WHERE teacherid IN
(SELECT tid FROM teacher WHERE tname='李平老师')))zo GROUP BY oo)oi ))oii WHERE oo=sid;


21、查询不同课程但成绩相同的学号，课程号，成绩
SELECT ppp 学号,qqq 分数,sname 姓名 FROM student,
(SELECT pp ppp,qq qqq FROM
(SELECT studentid pp,num qq,COUNT(num) ee FROM score GROUP BY studentid,num)dd WHERE ee>=2)da WHERE ppp=sid;


22、查询没学过“李平”老师课程的学生姓名以及选修的课程名称；
SELECT pu.up,pu.hp,pe.km FROM
(SELECT ui,hi,cname km FROM course,
(SELECT uu ui,hh hi,courseid oi FROM 
(SELECT sname uu,sid hh FROM student WHERE sid IN
(SELECT sid FROM student WHERE sid NOT IN
(SELECT studentid FROM score WHERE courseid IN
(SELECT cid FROM course WHERE teacherid IN
(SELECT tid FROM teacher WHERE tname='李平老师')))))op,score WHERE hh = score.studentid)ppi WHERE oi=cid)pe
LEFT OUTER JOIN 
(SELECT sname up,sid hp FROM student WHERE sid IN
(SELECT sid FROM student WHERE sid NOT IN
(SELECT studentid FROM score WHERE courseid IN
(SELECT cid FROM course WHERE teacherid IN
(SELECT tid FROM teacher WHERE tname='李平老师')))))pu ON up=up;


23、查询所有选修了学号为1的同学选修过的一门或者多门课程的同学学号和姓名；
SELECT sid,sname FROM student WHERE sid IN
(SELECT studentid pp FROM score WHERE courseid IN
(SELECT courseid FROM score WHERE studentid=1) AND studentid!=1);



24、任课最多的老师中学生单科成绩最高的学生姓名
SELECT sname FROM student WHERE sid IN
(SELECT studentid FROM score WHERE num=
(SELECT MAX(fn) FROM 
(SELECT num fn FROM score WHERE courseid IN
(SELECT cid FROM course WHERE teacherid=
(SELECT ii FROM 
(SELECT teacherid ii,COUNT(teacherid) po FROM course GROUP BY teacherid)oo WHERE po=
(SELECT MAX(pp) FROM 
(SELECT teacherid,COUNT(teacherid) pp FROM course GROUP BY teacherid)op))))cj) AND
courseid IN (SELECT cid FROM course WHERE teacherid=
(SELECT ii FROM 
(SELECT teacherid ii,COUNT(teacherid) po FROM course GROUP BY teacherid)oo WHERE po=
(SELECT MAX(pp) FROM 
(SELECT teacherid,COUNT(teacherid) pp FROM course GROUP BY teacherid)op))));